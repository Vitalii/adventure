var headerLink = document.querySelector('.header-link');
var head = document.querySelector('header');

headerLink.addEventListener('mouseover', function() {
	head.style.background = '#22539d';
});
headerLink.addEventListener('mouseout', function() {
	head.style.background = '#282a2d';
});



var elem = document.querySelector('.grid');
var msnry = new Masonry( elem, {
  // options
	itemSelector: '.grid-item',
	columnWidth: 40,
	isFitWidth: true,
	transitionDuration: '0.2s'
});




jQuery(document).ready(function(){
	function htmSlider(){
		var slideWrap = jQuery('.cuba-1');
		var nextLink = jQuery('.next-slide');
		var prevLink = jQuery('.prev-slide');
		var slideWidth = jQuery('.slide-item').outerWidth();		
		var newLeftPos = slideWrap.position().left - slideWidth;		
		
		nextLink.click(function(){
			if(!slideWrap.is(':animated')) {	
				slideWrap.animate({left: newLeftPos}, 200, function(){
					slideWrap
						.find('.slide-item:first')
						.appendTo(slideWrap)
						.parent()
						.css({'left': 0});
				});
			}
		});

		prevLink.click(function(){
			if(!slideWrap.is(':animated')) {
				slideWrap.animate({left: newLeftPos}, 200, function(){
				slideWrap
					.find('.slide-item:last')
					.prependTo(slideWrap)
					.parent()
					.css({'left': newLeftPos})
//					.animate({left: 0}, 300);
				});
			}
		});

	}

	htmSlider();
});




var play = document.querySelector('.play');
play.addEventListener('click', function() {
	var popUp = document.querySelector('.popup-dance');
	popUp.style.display = 'block';
});
var closeDance = document.querySelector('.close-dance');
closeDance.addEventListener('click', function() {
	var popUpDance = document.querySelector('.popup-dance');
	popUpDance.style.display = 'none';
})

var zoom = document.querySelector('.zoom');
var closePhoto = document.querySelector('.close-photo');
zoom.addEventListener('click', function() {
	var popUpPhoto = document.querySelector('.popup-photo');
	popUpPhoto.style.display = 'block';
});
closePhoto.addEventListener('click', function() {
	var popUpPhoto = document.querySelector('.popup-photo');
	popUpPhoto.style.display = 'none';
})



var allCountry = document.querySelector('.all-country');
var	cultute = document.querySelector('.culture');
var	extreme = document.querySelector('.extreme');
var	atmosphere = document.querySelector('.atmosphere');
var	people = document.querySelector('.people');
	

var cuba = document.querySelector('.cuba-1');
var	russia = document.querySelector('.russia');
var	latin = document.querySelector('.latin');
var	england = document.querySelector('.england');	
var	norway = document.querySelector('.norway');
var	switzerland = document.querySelector('.switzerland');
var	china = document.querySelector('.china');
var	germany = document.querySelector('.germany');
var	japan = document.querySelector('.japan');
var	australia = document.querySelector('.australia');
var	india = document.querySelector('.india');
var	finland = document.querySelector('.finland');
var	newZeland = document.querySelector('.new-zeland');
var countryArray = [cuba, russia, latin, england, norway, switzerland, china, germany, japan, australia, india, finland, newZeland];


var cubaImg = document.querySelector('.cuba-img'); 
var	russiaImg = document.querySelector('.russia-img'); 
var	latinImg = document.querySelector('.latin-img'); 
var	englandImg = document.querySelector('.england-img'); 
var	norwayImg = document.querySelector('.norway-img'); 
var	switzerlandImg = document.querySelector('.switzerland-img'); 
var	chinaImg = document.querySelector('.china-img'); 
var	germanyImg = document.querySelector('.germany-img'); 
var	japanImg = document.querySelector('.japan-img'); 
var	australiaImg = document.querySelector('.australia-img'); 
var	indiaImg = document.querySelector('.india-img'); 
var	finlandImg = document.querySelector('.finland-img'); 
var	newzealandImg = document.querySelector('.newzealand-img');

var allImgArray = [cubaImg, russiaImg, latinImg, englandImg, norwayImg, switzerlandImg, chinaImg, germanyImg, japanImg, australiaImg, indiaImg, finlandImg, newzealandImg];
var imgArray1 = [norwayImg, switzerlandImg, australiaImg, finlandImg, newzealandImg];	
var imgArray2 = [cubaImg, englandImg, norwayImg, chinaImg, germanyImg, japanImg, indiaImg, finlandImg, newzealandImg];	
var imgArray3 = [cubaImg, latinImg, englandImg, switzerlandImg, chinaImg, germanyImg, japanImg, australiaImg, indiaImg];	
var imgArray4 = [russiaImg, norwayImg, switzerlandImg, chinaImg, japanImg, australiaImg, indiaImg, finlandImg, newzealandImg];

allCountry.addEventListener('click', function() {	
	for (var i=0; i<allImgArray.length; i++) {
		allImgArray[i].style.cssText = 'width: inherit; height: inherit;';
	};
	
	for (var i=0; i<countryArray.length; i++) {
		countryArray[i].style.background = 'none';
	};
});


cultute.addEventListener('click', function() {
	for (var i=0; i<allImgArray.length; i++) {
		allImgArray[i].style.cssText = 'width: inherit; height: inherit;';
	};	
	for (var i=0; i<countryArray.length; i++) {
		countryArray[i].style.background = 'none';
	};	
	
	for (var j=0; j<imgArray1.length; j++) {
		imgArray1[j].style.cssText = 'width: 240px; height: 160px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';
	};	
	norway.style.background = '#ffffff';
	switzerland.style.background = '#ffffff';
	australia.style.background = '#ffffff';
	finland.style.background = '#ffffff';
	newZeland.style.background = '#ffffff';
	
});

extreme.addEventListener('click', function() {
	for (var i=0; i<allImgArray.length; i++) {
		allImgArray[i].style.cssText = 'width: inherit; height: inherit;';
	};	
	for (var i=0; i<countryArray.length; i++) {
		countryArray[i].style.background = 'none';
	};
	
	
	for (var j=0; j<imgArray2.length; j++) {
		imgArray2[j].style.cssText = 'width: 240px; height: 160px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';
	};
	latinImg.style.cssText = 'width: 580px; height: 380px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';	
	cuba.style.background = '#ffffff';
	latin.style.background = '#ffffff';
	england.style.background = '#ffffff';
	norway.style.background = '#ffffff';
	china.style.background = '#ffffff';
	germany.style.background = '#ffffff';
	japan.style.background = '#ffffff';
	india.style.background = '#ffffff';
	finland.style.background = '#ffffff';
	newZeland.style.background = '#ffffff';
});

atmosphere.addEventListener('click', function() {
	for (var i=0; i<allImgArray.length; i++) {
		allImgArray[i].style.cssText = 'width: inherit; height: inherit;';
	};	
	for (var i=0; i<countryArray.length; i++) {
		countryArray[i].style.background = 'none';
	};
	
	for (var j=0; j<imgArray3.length; j++) {
		imgArray3[j].style.cssText = 'width: 240px; height: 160px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';
	};
	latinImg.style.cssText = 'width: 580px; height: 380px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';	
	cuba.style.background = '#ffffff';
	latin.style.background = '#ffffff';
	england.style.background = '#ffffff';
	switzerland.style.background = '#ffffff';
	china.style.background = '#ffffff';
	germany.style.background = '#ffffff';
	japan.style.background = '#ffffff';
	australia.style.background = '#ffffff';
	india.style.background = '#ffffff';
});

people.addEventListener('click', function() {
	for (var i=0; i<allImgArray.length; i++) {
		allImgArray[i].style.cssText = 'width: inherit; height: inherit;';
	};	
	for (var i=0; i<countryArray.length; i++) {
		countryArray[i].style.background = 'none';
	};
	
	for (var j=0; j<imgArray4.length; j++) {
		imgArray4[j].style.cssText = 'width: 240px; height: 160px; position: absolute; top: 20px; left: 30px; opacity: 0.3;';
	};
	russia.style.background = '#ffffff';
	norway.style.background = '#ffffff';
	switzerland.style.background = '#ffffff';
	china.style.background = '#ffffff';
	japan.style.background = '#ffffff';
	australia.style.background = '#ffffff';
	india.style.background = '#ffffff';
	finland.style.background = '#ffffff';
	newZeland.style.background = '#ffffff';
	
});